package sentimentanalysis.ws;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import sentimentanalysis.common.Sentiment;

import javax.sql.DataSource;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 * Created with IntelliJ IDEA.
 * User: abhisheg
 * Date: 06/11/12
 * Time: 5:42 PM
 */

public class CacheTask {

    private JdbcTemplate jdbcTemplate;
    public static final String SENTI_QUERY = "select brand, count(*), sum(positive), " +
            "sum(neutral),sum(negative) from (select \n" +
            "brand,\n" +
            "case \n" +
            "when score > 0 then 1 else 0\n" +
            "end as positive,\n" +
            "case \n" +
            "when score = 0 then 1 else 0\n" +
            "end as neutral,\n" +
            "case \n" +
            "when score < 0 then 1 else 0\n" +
            "end as negative\n" +
            "from brand_tweets where score is not null and " +
            "creation_date >= DATE_SUB(creation_date, INTERVAL 1 WEEK) and brand = ?) " +
            "t  group by brand;";
    public static final String USER_COUNT_QUERY = "select \n" +
            "brand, \n" +
            "count(*) total_users, \n" +
            "sum(case when c > 1 then 1 else 0 end) more_than_once, \n" +
            "sum(case when c = 1 then 1 else 0 end) only_once \n" +
            "from (select brand, user_name u, count(*) c from brand_tweets where brand = ? " +
            "and creation_date >= DATE_SUB(creation_date, INTERVAL 1 WEEK) group by user_name ) t \n" +
            "group by t.brand;";
    public static final String TOP_TAGS = "select brand, tag, count from brand_tags where brand = ? and tag like '#%' order by count desc limit 20;";

    @Autowired
    public CacheTask(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }

    public Entity getData(String entity) {
        Logger.getLogger(CacheTask.class.getName()).log(Level.SEVERE, "getting data for " + entity);
        Entity entityVO = new Entity();
        Sentiment sentiment = jdbcTemplate.queryForObject(SENTI_QUERY, new Object[]{entity}, new RowMapper<Sentiment>() {
            @Override
            public Sentiment mapRow(ResultSet rs, int i) throws SQLException {
                Sentiment s = new Sentiment();
                int positive = rs.getInt(3);
                int neutral = rs.getInt(4);
                int negative = rs.getInt(5);
                int total = rs.getInt(2);
                Sentiment.Tweets tweets = new Sentiment.Tweets(positive, negative, neutral, total);
                s.setTweets(tweets);
                float positivePercentage = (float) positive / (float) total;
                float negativePercentage = (float) negative / (float) total;
                float neutralPercentage = (float) neutral / (float) total;
                Sentiment.Percentage percentage = new Sentiment.Percentage(positivePercentage, negativePercentage, neutralPercentage);
                s.setPercentage(percentage);
                return s;
            }
        });

        Map<String, Object> usersMap = jdbcTemplate.queryForMap(USER_COUNT_QUERY, entity);
        entityVO.setRepeat_users(((BigDecimal) usersMap.get("more_than_once")).intValue());
        entityVO.setUnique_users(((Long) usersMap.get("total_users")).intValue());
        entityVO.setEntity(entity);
        entityVO.setSentiment(sentiment);

        List<Map<String, Object>> tagsList = jdbcTemplate.queryForList(TOP_TAGS, entity);
        List<Tag> list = new ArrayList<Tag>();
        Float tagMaxValue = -1f;
        for (Map<String, Object> tagResult : tagsList) {
            Float count = ((Integer) tagResult.get("count")).floatValue();
            if (tagMaxValue == -1) {
                tagMaxValue = count;
            }
            count = count / tagMaxValue;     // normalizing count to range of 0-1
            String tag = ((String) tagResult.get("tag")).replaceAll("#", "");
            list.add(new Tag(tag, count));
        }
        entityVO.setTags(list);
        return entityVO;
    }
}