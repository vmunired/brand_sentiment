package sentimentanalysis.ws;

import sentimentanalysis.common.Sentiment;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: abhisheg
 * Date: 26/10/12
 * Time: 12:32 PM
 */
public class Entity {
    String entity;
    Sentiment sentiment;
    int unique_users;
    int repeat_users;
    List<Tag> tags;

    public List<Tag> getTags() {
        return tags;
    }

    public void setTags(List<Tag> tags) {
        this.tags = tags;
    }

    public String getEntity() {
        return entity;
    }

    public void setEntity(String entity) {
        this.entity = entity;
    }

    public Sentiment getSentiment() {
        return sentiment;
    }

    public void setSentiment(Sentiment sentiment) {
        this.sentiment = sentiment;
    }

    public int getUnique_users() {
        return unique_users;
    }

    public void setUnique_users(int unique_users) {
        this.unique_users = unique_users;
    }

    public int getRepeat_users() {
        return repeat_users;
    }

    public void setRepeat_users(int repeat_users) {
        this.repeat_users = repeat_users;
    }

}
