package sentimentanalysis.imports;

import org.quartz.CronTrigger;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.impl.StdSchedulerFactory;
import sentimentanalysis.common.Brand;

import java.text.ParseException;

/**
 * Created with IntelliJ IDEA.
 * User: abhisheg
 * Date: 20/10/12
 * Time: 9:53 PM
 */
public class Main {
    public static void main(String[] args) throws ParseException, SchedulerException {
        // configure scheduler
        // start scheduler
        Scheduler scheduler = new StdSchedulerFactory().getScheduler();
        scheduler.start();
        Brand[] brands = Brand.values();
        for (Brand brand : brands) {
            System.out.println(brand);
            JobDetail job = new JobDetail();
            job.setName(brand.getJobName());
            job.setJobClass(ImportJob.class);
            job.getJobDataMap().put("brand", brand);

            CronTrigger trigger = new CronTrigger();
            trigger.setName(brand.getTriggerName());
            trigger.setCronExpression("0/30 * * * * ?");

            scheduler.scheduleJob(job, trigger);
        }

    }
}
