package sentimentanalysis.common;

import org.apache.http.client.HttpClient;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.PoolingClientConnectionManager;

/**
 * Created with IntelliJ IDEA.
 * User: abhisheg
 * Date: 21/10/12
 * Time: 12:45 AM
 */
public class HttpUtil {

    static final PoolingClientConnectionManager cm = new PoolingClientConnectionManager();

    private HttpUtil() {
        cm.setDefaultMaxPerRoute(100);
        cm.setMaxTotal(100);
    }


    public static HttpClient getHttpClient() {
        return new DefaultHttpClient(cm);
    }

}
