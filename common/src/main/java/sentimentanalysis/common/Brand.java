package sentimentanalysis.common;

import java.net.URI;
import java.net.URISyntaxException;

/**
 * Created with IntelliJ IDEA.
 * User: abhisheg
 * Date: 20/10/12
 * Time: 10:00 PM
 */
public enum Brand {
    ADIDAS,
    NIKE,
    REEBOK,
    PUMA,
    THOUGHTWORKS;

    public String getJobName() {
        return name() + " Import Job";
    }

    public String getTriggerName() {
        return name() + " Import Job Trigger";
    }

    public URI getTwitterURL() throws URISyntaxException {
        return new URI("http://search.twitter.com/search.json?q=\'" +
                this + "\'&lang=en&rpp=30&since_id=" + SinceIdCache.get(this));
    }

}
