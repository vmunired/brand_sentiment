package sentimentanalysis.analysis.custom;


import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by IntelliJ IDEA.
 * User: abhisheg
 * Date: 31/10/12
 * Time: 3:32 PM
 */
public class Modifiers {
    private static HashMap<String, Double> modifiers;
    private static final String MODIFIERS_CSV = "modifiers.csv";

    private static Modifiers INSTANCE;

    public static Modifiers getINSTANCE() {
        if (INSTANCE == null) {
            INSTANCE = new Modifiers();
        }
        return INSTANCE;
    }


    private Modifiers() {
        if (modifiers == null) {
            try {

                InputStream is;
                is = Lexicons.class.getClassLoader().getResourceAsStream(MODIFIERS_CSV);
                BufferedReader br = new BufferedReader(new InputStreamReader(is));
                String strLine;
                modifiers = new HashMap<String, Double>();
                while ((strLine = br.readLine()) != null) {
                    String[] split = strLine.split(",");
                    modifiers.put(split[0], Double.valueOf(split[1]));
                }
                is.close();
            } catch (Exception e) {
                Logger.getLogger(Modifiers.class.getName()).log(Level.SEVERE, null, e);
                modifiers = null;
            }
            Logger.getLogger(Modifiers.class.getName()).log(Level.INFO, "loaded " +
                    (modifiers == null ? 0 : modifiers.size()) + " keywords form filename " + MODIFIERS_CSV);
        }
    }


    public Double get(String word) {
        if (modifiers.containsKey(word))
            return modifiers.get(word);
        return 0.0;
    }


    public boolean contains(String token) {
        return modifiers.containsKey(token);
    }


}
