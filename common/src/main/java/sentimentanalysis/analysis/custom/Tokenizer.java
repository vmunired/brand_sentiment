package sentimentanalysis.analysis.custom;

import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.standard.StandardTokenizer;
import org.apache.lucene.analysis.tokenattributes.TermAttribute;
import sentimentanalysis.common.TweetVO;

import java.io.*;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class Tokenizer {


    /**
     * Retrieve the tokens in a Tweet. These are returned as a linked list of
     * strings; each string is a token, and they are ordered as they occur in
     * the tweet.
     * <p/>
     * Before the tokenization occurs, some preprocessing is performed on the
     * string, to properly convert URL's.
     *
     * @param tweet The tweet to tokenize.
     * @return The tokens in the tweets text.
     */
    public LinkedList<String> getTokens(TweetVO tweet) throws IOException {
        String text = TextNormalizer.getTweetWithoutUrlsAnnotations(tweet.getText());
        //System.out.println(getTokens(text));
        return getTokens(text);
    }


    /**
     * Retrieve the tokens in a String. Behaves like getTokens, but operates on
     * a string instead of a tweet object.
     *
     * @param text The text to tokenize.
     * @return The tokens in the text.
     * @throws java.io.IOException ex
     */

    // Version 1
    /*public LinkedList<String> getTokens (String text) {
        LinkedList<String> tokens   = new LinkedList();
        String[] words              = text.split(" ");
        tokens.addAll(Arrays.asList(words));
        return tokens;
    }*/


    static List<String> stopWords=null;

    private static void getStopWords(){
        stopWords=new ArrayList<String>();
        try {
            System.out.println("loading file >>>>>>>>>>>> " + "stopwords.txt");
            InputStream is;
            is = Tokenizer.class.getClassLoader().getResourceAsStream("stopwords.txt");
            BufferedReader br = new BufferedReader(new InputStreamReader(is));
            String strLine;
            while ((strLine = br.readLine()) != null) {
                stopWords.add(strLine);
            }
            is.close();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    // Version 2
    public static LinkedList<String> getTokens(String text) throws IOException {
        if(stopWords==null){
            getStopWords();
        }
        LinkedList<String> tokens = new LinkedList<String>();
        TokenStream ts = new StandardTokenizer(new StringReader(text));
        TermAttribute termAtt = (TermAttribute) ts.getAttribute(TermAttribute.class);
        while (ts.incrementToken()) {
            String term = termAtt.term().toLowerCase();
            if(Lexicons.getINSTANCE().get(term)!=0)
                tokens.add(term);
            else if(Negations.getINSTANCE().get(term)!=0)
                tokens.add(term);
            else if(Modifiers.getINSTANCE().get(term)!=0)
                tokens.add(term);
            else if(!stopWords.contains(term))
                tokens.add(term);
        }
        System.out.println(tokens);
        return tokens;
    }

}