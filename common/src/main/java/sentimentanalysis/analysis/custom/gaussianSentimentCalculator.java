package sentimentanalysis.analysis.custom;

import sentimentanalysis.analysis.SentimentCalculator;
import sentimentanalysis.common.TweetVO;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;


public class GaussianSentimentCalculator implements SentimentCalculator {

    private Tokenizer tokenizer = null;
    private double[] gaussian = new double[6];


    public GaussianSentimentCalculator() throws Exception {
        this.tokenizer = new Tokenizer();

    }

    private double getSentiment(String entity, TweetVO tweet) throws IOException {
        LinkedList<String> tokens = tokenizer.getTokens(tweet);
        Double score = 0.0;
        Double negation = 1.0;
        Double modifier = 1.0;
        int range = 0;
        Double gauss;
        int i = 0;
        for (String token : tokens) {
            gauss = getDistance(entity, token, tokens, i, this.gaussian);
            score += (negation) * (modifier) * (gauss) * Lexicons.getINSTANCE().get(token);
            // Deal with negations
            if (isNegation(token)) {
                negation = -1.0;
                range = 2;
            } else if (range > 1)
                range--;
            else
                negation = 1.0;
            modifier = getModifierValue(token, negation);
            i++;
        }
        return score;
    }

    private static boolean isNegation(String token) {
        return Negations.getINSTANCE().contains(token);
    }


    private static double getModifierValue(String token, Double negation) {
        double m;
        if (Modifiers.getINSTANCE().contains(token)) {
            m = Modifiers.getINSTANCE().get(token);
            if ((negation == -1.0) && (m == 2))
                m = 0.8;
        } else
            m = 1.0;
        return m;
    }


    public double[] CalculateDistance() {
        double sigma = 1.0;
        double gaussian_max = Math.exp(-(1 / (sigma * sigma * 2)))
                / (sigma * Math.sqrt(2 * Math.PI));
        for (int i = 0; i <= 5; i++) {
            gaussian[i] = (1 / gaussian_max) *
                    Math.exp(-(((i) / sigma) * ((i) / sigma)) / 2) /
                    (sigma * Math.sqrt(2 * Math.PI));
        }
        return gaussian;
    }

    private double getDistance(String entity, String token, LinkedList<String> tokens,
                               int i, double gaussian[]) {
        int entityIndex = -1;
        for (int j = 0; j < tokens.size(); j++) {
            if (tokens.get(j).contains(entity)) {
                entityIndex = j;
                break;
            }
        }
        if (entityIndex < 0) return 0.0;

        double height = Math.abs(Lexicons.getINSTANCE().get(token));
        if (Math.abs(entityIndex - i) <= 5)
            return height * gaussian[Math.abs(entityIndex - i)];
        else
            return 0.0;
    }


    @Override
    public Double get(String entity, String text) throws IOException {
        TweetVO tweetVO = new TweetVO();
        tweetVO.setText(text);
        return getSentiment(entity, tweetVO);
    }

    @Override
    public void populateScore(String entity, List<TweetVO> tweets) throws IOException {
        entity = entity.toLowerCase();
        double score;
        this.gaussian = CalculateDistance();
        for (TweetVO tweet : tweets) {
            tweet.setText(tweet.getText().toLowerCase());
            score = getSentiment(entity, tweet);
            tweet.setScore(score);
        }
    }

}