package sentimentanalysis.analysis.custom;


import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by IntelliJ IDEA.
 * User: abhisheg
 * Date: 31/10/12
 * Time: 3:32 PM
 */
public class Negations {
    private static HashMap<String, Double> negations;
    private static final String NEGATIONS_CSV = "negations.csv";
    private static Negations INSTANCE;

    public static Negations getINSTANCE() {
        if (INSTANCE == null) {
            INSTANCE = new Negations();
        }
        return INSTANCE;
    }


    private Negations() {
        if (negations == null) {
            try {
                InputStream is;
                is = Lexicons.class.getClassLoader().getResourceAsStream(NEGATIONS_CSV);
                BufferedReader br = new BufferedReader(new InputStreamReader(is));
                String strLine;
                negations = new HashMap<String, Double>();
                while ((strLine = br.readLine()) != null) {
                    String[] split = strLine.split(",");
                    negations.put(split[0], Double.valueOf(split[1]));
                }
                is.close();
            } catch (Exception e) {
                Logger.getLogger(Negations.class.getName()).log(Level.SEVERE, null, e);
                negations = null;
            }

            Logger.getLogger(Negations.class.getName()).log(Level.INFO, "loaded " +
                    (negations == null ? 0 : negations.size()) + " keywords form filename " + NEGATIONS_CSV);
        }
    }


    public Double get(String word) {
        if (negations.containsKey(word))
            return negations.get(word);
        return 0.0;
    }


    public boolean contains(String token) {
        return negations.containsKey(token);
    }
}
