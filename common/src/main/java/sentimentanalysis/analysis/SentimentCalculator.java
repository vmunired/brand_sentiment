package sentimentanalysis.analysis;

import sentimentanalysis.common.TweetVO;

import java.io.IOException;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: abhisheg
 * Date: 31/10/12
 * Time: 12:48 PM
 */
public interface SentimentCalculator {

    Double get(String entity, String text) throws IOException;

    void populateScore(String entity, List<TweetVO> results) throws IOException;
}
