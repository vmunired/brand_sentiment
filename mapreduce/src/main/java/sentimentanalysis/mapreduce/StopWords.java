package sentimentanalysis.mapreduce;


import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;


public class StopWords {

    private static List<String> stopWords;
    private static final String LEXICON_CSV = "stopwords.txt";
    private static StopWords INSTANCE;

    public static StopWords getINSTANCE() {
        if (INSTANCE == null) {
            INSTANCE = new StopWords();
        }
        return INSTANCE;
    }


    private StopWords() {
        if (stopWords == null) {
            try {
                System.out.println("loading file >>>>>>>>>>>> " + LEXICON_CSV);
                InputStream is;
                is = StopWords.class.getClassLoader().getResourceAsStream(LEXICON_CSV);
                BufferedReader br = new BufferedReader(new InputStreamReader(is));
                String strLine;
                stopWords = new ArrayList<String>();
                while ((strLine = br.readLine()) != null) {
                    stopWords.add(strLine);
                }
                is.close();
            } catch (Exception e) {
                e.printStackTrace();
                stopWords = null;
            }
            System.out.println("loaded " +
                    (stopWords == null ? 0 : stopWords.size()) + " keywords form filename " + LEXICON_CSV);
        }
    }


    public boolean contains(String word) {
        return stopWords.contains(word.toLowerCase());
    }


}
